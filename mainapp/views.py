from django.shortcuts import render
from django.conf import settings
from django.conf.urls.static import static
from django.core.mail import send_mail
import time
import requests
import json
from django.contrib import messages
from .models import Customizer
import apiform.forms
from django.contrib import messages
from datetime import datetime, timedelta


def home(request):
    meta = {}
    utm_custom = request.GET.get('utm_custom', False)
    if utm_custom != 0:
        mycontent = Customizer.objects.get(utm=utm_custom)
        meta = {
            'title': mycontent.text + ' - бесплатная консультация юриста онлайн',
            'description':  mycontent.text + ' - бесплатная консультация юриста онлайн',
            'company': 'АНОНИМНЫЙ Центр юридической помощи гражданам г. Москвы',
            'lead': 'Юридическая консультация по теме «' + mycontent.text + '»',
        }
    else:
        meta = {
            'title': 'Юридическая консультация онлайн',
            'description': 'Бесплатно, ответ за 7 минут, юристы со стажем 10 лет',
            'company': 'Центр АНОНИМНОЙ юридической помощи гражданам г. Москвы',
            'lead': 'Бесплатная анонимная консультация юриста',
        }
    # form
    if request.method == "POST":
        form = apiform.forms.ContactForm(request.POST)
        if form.is_valid():
            phone = form.cleaned_data['phone']
            fckspamers = form.cleaned_data['fckspamers']
            question = form.cleaned_data['question']
            myparams = {
                'wm_id': '7518',
                'phone': phone,
                'location': 'Москва',
                'question': question,
            }
            form = apiform.forms.ContactForm()
            if fckspamers == '':
                r = requests.get('http://api.lexprofit.ru/v1', params=myparams)
                response = json.loads(r.text)
                print(response)
                response.setdefault('success', False)
                if response['success']:
                    messages.add_message(
                        request, messages.INFO, 'Спасибо за обращение. Ваша заявка принята. В ближайшее время вам позвонит наш юрист и всё расскажет.')
                else:
                    messages.add_message(
                        request, messages.INFO, 'Произошла ошибка. Проверьте номер телефона!')
    else:
        form = apiform.forms.ContactForm()
    # time
    now = datetime.now()
    questions = []
    questions.append(datetime.now() - timedelta(minutes=5))
    questions.append(datetime.now() - timedelta(minutes=17))
    answers = []
    answers.append(datetime.now() - timedelta(minutes=3))
    answers.append(datetime.now() - timedelta(minutes=13))
    context = {
        'form': form,
        'debug': meta,
        'questions': questions,
        'answers': answers,
    }
    return render(request, 'mainapp/home.html', context)
