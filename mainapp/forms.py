from django import forms


class ContactForm(forms.Form):
    use_required_attribute = True
    phone = forms.CharField(widget=forms.TextInput(
        attrs={"class": "form-control mt-3", 'placeholder': 'ваш номер'}), max_length=100, required=True)
    question = forms.CharField(widget=forms.Textarea(
        attrs={"class": "form-control", 'rows': 5, 'placeholder': 'Напишите ваш вопрос.\nНапример: После развода у меня остается двое детей. Как добиться алиментов от бывшего мужа?'}), required=True)

    def clean_question(self):
        data = self.cleaned_data['question']
        if len(data) < 10:
            raise forms.ValidationError(
                "Опишите ваш вопрос. Как минимум 10 символов. Вы ввели " +
                str(len(data)))
        # Always return a value to use as the new cleaned data, even if
        # this method didn't change it.
        return data

    def clean_phone(self):
        data = self.cleaned_data['phone']
        if "8888" not in data:
            raise forms.ValidationError(
                "Телефон должен содержать 10 цифр +75557772233")
        # Always return a value to use as the new cleaned data, even if
        # this method didn't change it.
        return data
