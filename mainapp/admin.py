from django.contrib import admin

from .models import Customizer

admin.site.register(Customizer)