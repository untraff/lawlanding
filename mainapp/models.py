from django.db import models


class Customizer(models.Model):
    utm = models.CharField(max_length=255)
    text = models.CharField(max_length=255)
