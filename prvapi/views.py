from django.shortcuts import render
import requests
import json
import pprint
# from django.http import HttpResponseRedirect
# from django.conf import settings
# from django.conf.urls.static import static
# from .forms import ContactForm
# from django.core.mail import send_mail


def prvapi(request):
    phone = '+79216556310'
    question_text = 'ТЕСТОВЫЙ ВОПРОС'

    myparams = {
        'd': 'jsonp',
        'event_type': 'lead_market_lead',
        'edata[type]': 'addLeadCd',
        'edata[type_id]': '0',
        'edata[city_id]': '0',
        'edata[name]': 'Клиент',
        'edata[phone]': phone,
        'edata[question_text]': question_text,
        'cd-referral': 'fb1a27a6e2cbe3ae08bfbf1960353af1',
        'putm_content': 'form',
        'putm_medium': 'https://infolaw.ru/',
        'data2': '1543992280572102326',
        'edata[user_agent]': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
    }
    r = requests.get('https://pravoved.ru/polling/', params=myparams)
    debug = r.text
    return render(request, 'prvapi/home.html', {'debug': debug, })
