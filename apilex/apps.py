from django.apps import AppConfig


class ApilexConfig(AppConfig):
    name = 'apilex'
