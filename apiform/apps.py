from django.apps import AppConfig


class ApiformConfig(AppConfig):
    name = 'apiform'
