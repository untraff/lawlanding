from django import forms
import re


class ContactForm(forms.Form):
    use_required_attribute = True
    phone = forms.CharField(label='', widget=forms.TextInput(
        attrs={"class": "form-control mt-3", 'placeholder': 'ваш номер телефона 89...'}), max_length=100, required=True)
    question = forms.CharField(label='', widget=forms.Textarea(
        attrs={"class": "form-control", 'rows': 5, 'placeholder': 'Задайте ваш вопрос юристу.\nНапример: Разводимся с мужем. Есть сын 3 года. Какой будет размер алиментов?'}), required=True)
    fckspamers = forms.CharField(label='', widget=forms.Textarea(
        attrs={"class": "form-control d-none", 'placeholder': 'мы не любим спам', 'rows': '1'}), max_length=100, required=False)

    def clean_phone(self):
        data = self.cleaned_data['phone']
        digits = re.findall('\d', data)
        if len(digits) < 10 or len(digits) > 11:
            raise forms.ValidationError(
                "Телефон должен содержать от 9 до 11 цифр, например +79214007030, а вы ввели " + str(len(digits)))
        return data

    def clean_question(self):
        data = self.cleaned_data['question']
        if len(data) < 10:
            raise forms.ValidationError(
                "Пожалуйста, расскажите о вашем вопросе немного подробнее")
        return data
